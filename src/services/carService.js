class CarService {
    /**
     * Adiciona uma oferta ao veículo referenciado
     *
     * @param {Array<any>} cars
     * @param {number} index
     * @returns {Array<any>}
     */
    addOffer(cars, index) {
        const actualCars = cars;
        const lastBid = actualCars[index].bids[actualCars[index].bids.length - 1];
        actualCars[index].bids.push({
            ...lastBid,
            amount: lastBid ? lastBid.amount + 250 : 250,
            createdAt: new Date().toISOString(),
        });
        return actualCars;
    }

    /**
     * Atualiza os segundos restantes dos carros
     *
     * @param {Array<any>} cars
     * @returns {Array<any>}
     */
    controlSeconds(cars) {
        return cars
            .map((car) => {
                return { ...car, remainingTime: car.remainingTime - 1 };
            })
            .filter((car) => car.remainingTime > 0);
    }

    /**
     * Converte o tempo restante de milisegundos para segundos
     *
     * @param {Array<any>} cars
     * @returns {Array<any>}
     */
    mapTimeInSeconds(cars) {
        return cars.map((car) => {
            return { ...car, remainingTime: car.remainingTime / 1000 };
        });
    }

    /**
     * Ordena os carros com o tempo restante de forma ascendente
     *
     * @param {Array<any>} cars
     * @returns {Array<any>}
     */
    orderAscendentTime(cars) {
        return cars.sort((car, nextCar) => {
            return car.remainingTime < nextCar.remainingTime ? -1 : car.remainingTime > nextCar.remainingTime ? 1 : 0;
        });
    }

    /**
     * Pega a lista de carros
     *
     * @returns {Array<any>}
     */
    async get(): Promise<Array<any>> {
        try {
            const request = await fetch(
                "https://s3-sa-east-1.amazonaws.com/config.instacarro.com/recruitment/auctions.json"
            );
            return await request.json();
        } catch (error) {
            throw error;
        }
    }
}
export default new CarService();
