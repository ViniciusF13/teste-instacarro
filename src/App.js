import "./App.css";
import { Card, Header } from "./components";
import { useEffect, useState } from "react";
import { CarService } from "./services";

function App() {
    const [cars, setCars] = useState([]);

    useEffect(() => {
        init();
    }, []);

    useEffect(() => {
        const myInterval = setInterval(() => {
            setCars(CarService.controlSeconds(cars));
        }, 1000);
        return () => {
            clearInterval(myInterval);
        };
    }, [cars]);

    const init = async () => {
        try {
            let carsLoaded = await CarService.get();
            carsLoaded = CarService.mapTimeInSeconds(carsLoaded);
            carsLoaded = CarService.orderAscendentTime(carsLoaded);
            setCars(carsLoaded);
        } catch (error) {
            setCars([]);
            alert("Não foi possível carregar os carros no momento, tente novamente mais tarde.");
        }
    };

    const makeOffer = (index) => {
        setCars(CarService.addOffer(cars, index));
    };

    return (
        <div className="App">
            <Header />
            <div className="body">
                {cars.map((car, key) => (
                    <Card car={car} makeOffer={makeOffer} index={key} key={key} />
                ))}
            </div>
        </div>
    );
}

export default App;
