export default function formatSecondsToHours(value: string | number): string {
    const sec = parseInt(value, 10);
    let hours = Math.floor(sec / 3600);
    let minutes = Math.floor((sec - hours * 3600) / 60);
    let seconds = sec - hours * 3600 - minutes * 60;

    /**
     * Checks if the value needs complement
     *
     * @param {string|number} val
     * @returns {string}
     */
    const checkVal = (val) => {
        return val < 10 ? "0" + val : val;
    };

    hours = checkVal(hours);
    minutes = checkVal(minutes);
    seconds = checkVal(seconds);

    return hours === 0 ? minutes + ":" + seconds : hours + ":" + minutes + ":" + seconds;
}
