import React from "react";
import "./index.css";
import logo from "../../assets/logo.png";
import phone from "../../assets/phone.png";
import user from "../../assets/user.png";
import caret from "../../assets/caret.png";

const Header = () => {
    return (
        <header className="header">
            <img src={logo} className="img logo" alt="logo" />
            <img src={phone} className="img phone" alt="phone" />
            (11) 3569 - 3465
            <div className="user-box">
                <img src={user} alt="user" />
                <img src={caret} className="caret" alt="menu" />
                <span>User Test</span>
            </div>
        </header>
    );
};

export default Header;
