import React from "react";
import formatSecondsToHours from "../../hooks/formatSecondsToHours";
import "./index.css";

const Card = ({ car, makeOffer, index }) => {
    return (
        <div className="card">
            <div className="img">
                <img src={car.imageUrl} alt="" />
                <span>ver detalhes</span>
            </div>
            <div className="content">
                <table>
                    <tbody>
                        <tr>
                            <td className="border w-50">
                                TEMPO RESTANTE
                                <br />
                                <span className="info time">{formatSecondsToHours(car.remainingTime)}</span>
                            </td>
                            <td className="w-50">
                                ULTIMA OFERTA
                                <br />
                                <span className="info value">
                                    R${" "}
                                    {car.bids.length ? car.bids[car.bids.length - 1].amount.toLocaleString("pt-BR") : 0}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2" className="detail">
                                {car.make} {car.model} {car.version} {car.year}
                            </td>
                        </tr>
                        <tr className="info-numbers">
                            <td className="w-50 border">{car.year}</td>
                            <td className="w-50">{car.km.toLocaleString("pt-BR")} KM</td>
                        </tr>
                    </tbody>
                </table>
                <button onClick={() => makeOffer(index)}>FAZER OFERTA</button>
            </div>
        </div>
    );
};

export default Card;
